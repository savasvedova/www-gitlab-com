---
layout: handbook-page-toc
title: "List Imports"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Types of Imports 

* [Field & Corporate Event Import](#field-and-corporate-event-import)
* [Ad Hoc Import](#ad-hoc-import)
* [Acceleration Team Monthly Import](#acceleration-team-monthly-import)

### Field and Corporate Event Import

TBA

### Ad Hoc Import

TBA

### Acceleration Team Monthly Import

The Acceleration Team has list import requests every month for their target patches. They presently are using a modified version of the Ad Hoc import template. 

**Key Differences**
- SLA of 5 business days *does not* apply to the monthly imports as there are multiple lists per issue & multiple issues per month
- Lists are raw data dumps from DiscoverOrg & not cleaned up 

In Marketo & SFDC there are two programs/campaigns set up to handle these imports. If you are coordinating efforts with another Ops team member be sure to clarify who is using which program/campaign to avoid commingling of records.

Please **do not** make any changes to the programs, campaigns or related workflows without talking to MktgOps first. 

##### Acceleration Upload - Ops 1
{:.no_toc}

* [Marketo Program](https://app-ab13.marketo.com/#PG3938A1)
* [Salesforce Campaign](https://gitlab.my.salesforce.com/7014M000001lnDr)
* [CONTACT view](https://gitlab.my.salesforce.com/003?fcf=00B4M000004oVs8)
* [LEAD view](https://gitlab.my.salesforce.com/00Q?fcf=00B4M000004oVvM)

##### Acceleration Upload - Ops 2
{:.no_toc}

* [Marketo Program](https://app-ab13.marketo.com/#PG4142A1)
* [Salesforce Campaign](https://gitlab.my.salesforce.com/7014M000001loEg)
* [CONTACT view](https://gitlab.my.salesforce.com/003?fcf=00B4M000004oXqs)
* [LEAD view](https://gitlab.my.salesforce.com/00Q?fcf=00B4M000004oXqn)

#### Process

1. Verify that you have `Edit` access to all of the GSheets shared by the Acceleration team, if not ping the Sheet owner in the issue and request.
1. Clean up list to remove any columns not needed, update [`Field Names`](https://docs.google.com/spreadsheets/d/1dkh715tPngbY29PZis02dNPNd_Uc-xGbfV7LghsKWqE/edit?usp=sharing) to Marketo compatible values. 
     - Sort list by `Email Address` -> remove any lines without `Email Address` provided (we do not upload any record w/o `Email Address` it is our unique identifier across all systems)
     - Sort list by `Phone` -> check to ensure all values are correct format (plain text, XXX-XXX-XXXX or (XXX) XXX-XXXX), if there is an extension it should be obvious its an extension with either an `x` or `ext.` 
     - Sort list by `Country` -> if `Country` **does not** equal `United States` or `Canada` remove the value in `State`
     - Sort list by `Billing Country` -> if `Billing Country` **does not** equal `United States` or `Canada` remove the value in `Billing State` 
     - Update `State` and `Billing State` values to be full name not abbreviation
     - Select all -> **Data** Remove duplicates -> check "Data has header row" -> only select `Email Address` field as the duplicate value to analyze
1. Once list is clean in GSheet download as csv
1. **IMPORTANT** make sure both the SFDC & Marketo list are entirely empty & program has **NO** members *before* starting import. 
1. In **Marketo**, update the Smart Campaign - Flow Step 6 `Sync Person to SFDC`. **Assign to** needs to match the list owner as the campaign will trigger as soon as the list is uploaded.   
1. Click on the List in `List Actions` menu click **Import List**
1. In the lightbox that appears: 
     - Select `Browse:` and choose the csv list you just downloaded
     - Leave all other dropdowns with default values
     - Select `Next`
     - Review the `Marketo Field` matches
          - If there are any `--IGNORE--` use drop down to update selecting correct field, unless they are intentionally skipped
     - Select `Next`
     - `Aquisition Program` = `Acceleration Upload - Ops 1` or `Acceleration Upload - Ops 2` (we do not use Marketo Revenue Modeler but please fill in for accuracy)
     - Select `Import`
1. **Marketo** import will be running and associated Smart Campaign will immediately trigger. 
1. Wait for the sync between Marketo<>SFDC to finished processing list
1. In **SFDC**, the `Responses in Campaign` should MATCH the number of records uploaded into Marketo
1. Use the `Acceleration Upload` LEAD and CONTACT views to verify:
     - `Owner` matches the Acceleration SDR owning the list
     - `Person Source` value is not blank &/or is set to `DiscoverOrg`
     - `Status` equals `Raw` if net new record
1. From **SFDC** you will **Import to Outreach**
     - Select all records in **SFDC**
     - Click `Import to Outreach` button
     - **Outreach Everywhere** window will open - may ask to import records click "import" -> if errors see troubleshooting section
     - Select all records in **Outreach Everywhere**, if more than 50 you will need to click link to "apply to all" 
     - Click the `...` and select `Add tag` - first import you will need to paste requested tag from issue; all subsequent imports you can select the tag
     - Close window when done
1. Repeat above step for all records in both views 
1. Navigate to new tab with **Outreach** open
1. Click to `Prospects` and clear all default filters
1. In left side menu, navigate to `Tags` and select the appropriate tag for that list
1. Copy the **Outreach** URL and paste into the List Import issue.  
1. Repeat steps above for each list in the issue. 